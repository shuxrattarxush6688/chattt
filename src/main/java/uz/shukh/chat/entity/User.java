package uz.shukh.chat.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.shukh.chat.enums.Status;
import uz.shukh.chat.enums.UserRole;

import javax.persistence.*;
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
@Data
public class User extends BaseEntity {

    private String fullName;

    private String username;

    private String password;

    @Enumerated(EnumType.STRING)
    private UserRole role = UserRole.USER;

    @Enumerated(EnumType.STRING)
    private Status status = Status.OFFLINE;

}

