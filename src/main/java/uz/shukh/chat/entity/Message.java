package uz.shukh.chat.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "message")
public class Message extends BaseEntity {

    @ManyToOne private User fromUser;

    @ManyToOne private User toUser;

    private String text;

    @ManyToOne
    private Chat chat;

}

