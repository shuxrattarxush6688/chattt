package uz.shukh.chat.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
@Data

public class Chat extends BaseEntity {

    @ManyToOne
    private User meUser;

    @ManyToOne
    private User toUser;

    private String name;

    public User getMeUser() {
        return meUser;
    }

    public void setMeUser(User meUser) {
        this.meUser = meUser;
    }

    public User getTo() {
        return toUser;
    }

    public void setTo(User to) {
        this.toUser = to;
    }
}

