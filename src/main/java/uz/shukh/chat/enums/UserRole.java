package uz.shukh.chat.enums;

public enum UserRole {
    ADMINISTRATOR, USER
}
