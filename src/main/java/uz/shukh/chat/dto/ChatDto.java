package uz.shukh.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.shukh.chat.entity.Chat;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatDto {
    public UserDto toUser;
    public String name;
    public Long id;

    public static ChatDto toDto(Chat chat) {
        return new ChatDto(UserDto.toDto(chat.getToUser()), chat.getName(), chat.getId());
    }
}
