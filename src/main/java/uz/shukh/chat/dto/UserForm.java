package uz.shukh.chat.dto;

import lombok.Data;
import uz.shukh.chat.entity.User;

@Data
public class UserForm {
    public String fullName;
    public String username;
    private String password;
}
