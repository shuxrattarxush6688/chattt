package uz.shukh.chat.dto;

import lombok.Data;

@Data
public class MessageForm {
    public String text;
    public Long chatId;
}
