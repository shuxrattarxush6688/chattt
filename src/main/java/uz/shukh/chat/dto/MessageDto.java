package uz.shukh.chat.dto;

import lombok.Data;
import uz.shukh.chat.entity.Message;
import uz.shukh.chat.enums.SenderType;

import java.security.Principal;

@Data
public class MessageDto {
    public String text;
    public SenderType sender;
    public Long sendDate;

    public static MessageDto toDto(Message message, Principal principal) {
        MessageDto dto = new MessageDto();
        dto.sender = SenderType.YOU;
        if (message.getFromUser().getUsername().equals(principal.getName())) {
            dto.sender = SenderType.ME;
        }
        dto.setText(message.getText());
        dto.setSendDate(message.getCreatedDate().getTime());
        return dto;
    }
}
