package uz.shukh.chat.dto;

import lombok.Data;
import uz.shukh.chat.entity.User;

@Data
public class UserDto {
    public Long id;
    public String fullName;
    public String username;

    public static UserDto toDto(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setFullName(user.getFullName());
        dto.setUsername(user.getUsername());
//        dto.setRole(user.getRole());
        return dto;
    }
}
