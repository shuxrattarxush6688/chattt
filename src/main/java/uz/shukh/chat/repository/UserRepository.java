package uz.shukh.chat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.shukh.chat.entity.User;
import uz.shukh.chat.enums.Status;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByUsername(String username);

    User findFirstByUsername(String username);

    boolean existsByUsername(String username);

    List<User> findAllByStatus(Status status);



}
