package uz.shukh.chat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.shukh.chat.entity.Message;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message>findAllByChatId(Long chatId);
}
