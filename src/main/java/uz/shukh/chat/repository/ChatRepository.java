package uz.shukh.chat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.shukh.chat.entity.Chat;

import java.util.List;

public interface ChatRepository extends JpaRepository<Chat, Long> {

    Chat findByMeUserIdAndToUserId(Long me, Long to);

    List<Chat> findAllByMeUserUsername(String myUsername);

}
