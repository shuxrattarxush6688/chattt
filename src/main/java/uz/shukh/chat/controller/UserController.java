package uz.shukh.chat.controller;

import org.springframework.web.bind.annotation.*;
import uz.shukh.chat.dto.UserDto;
import uz.shukh.chat.dto.UserForm;
import uz.shukh.chat.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/registration")
public class UserController {
    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @PostMapping
    public UserDto add(@RequestBody UserForm dto){
            return service.add(dto);
    }

    @GetMapping
    public List<UserDto> getOnlineStatus(){
        return service.fidAllByOnlineUsers();
    }

    @PutMapping("update/{id}")
    public UserDto update(@RequestBody UserForm form, @PathVariable Long id){
        return service.update(form, id);
    }

    @PutMapping("{id}")
    public String onlineOffline(@PathVariable Long id){
         service.statusOnOff(id);
         return "Completed!";
    }


}
