package uz.shukh.chat.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.shukh.chat.dto.ChatForm;
import uz.shukh.chat.service.ChatService;

import java.security.Principal;

@RestController
@RequestMapping("/api/v1/chat")
public class ChatController {

    private final ChatService chatService;

    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @PostMapping("me")
    public Object sendMe(@RequestBody ChatForm form, Principal principal) {
        return chatService.create(form, principal);
    }
}
