package uz.shukh.chat.controller;

import org.springframework.web.bind.annotation.*;
import uz.shukh.chat.dto.MessageDto;
import uz.shukh.chat.dto.MessageForm;
import uz.shukh.chat.dto.ResultDto;
import uz.shukh.chat.service.MessageService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService service;

    public MessageController(MessageService service) {
        this.service = service;
    }

    @PostMapping
    public ResultDto send(@RequestBody MessageForm form, Principal principal) throws Exception {
        service.sendMessage(form, principal);
        return new ResultDto();
    }

    @GetMapping("{chatId}")
    public List<MessageDto> getAll(@PathVariable Long chatId, Principal principal){
        return service.getMessages(chatId, principal);
    }

}
