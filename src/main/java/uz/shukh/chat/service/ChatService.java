package uz.shukh.chat.service;

import org.springframework.stereotype.Service;
import uz.shukh.chat.dto.ChatDto;
import uz.shukh.chat.dto.ChatForm;
import uz.shukh.chat.entity.User;

import java.security.Principal;
import java.util.List;

@Service
public interface ChatService {

    Object create(ChatForm form, Principal principal);

    List<ChatDto> findAll(Principal principal);


}
