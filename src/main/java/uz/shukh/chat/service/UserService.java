package uz.shukh.chat.service;

import uz.shukh.chat.dto.UserDto;
import uz.shukh.chat.dto.UserForm;

import java.util.List;

public interface UserService {

    UserDto add(UserForm form);

    List<UserDto> fidAllByOnlineUsers();

    UserDto update(UserForm form, Long id);

    void statusOnOff(Long id);

}
