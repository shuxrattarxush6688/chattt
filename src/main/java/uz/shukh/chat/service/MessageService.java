package uz.shukh.chat.service;
import uz.shukh.chat.dto.MessageDto;
import uz.shukh.chat.dto.MessageForm;

import java.security.Principal;
import java.util.List;

public interface MessageService {

    Object sendMessage(MessageForm form, Principal principal) throws Exception;

     List<MessageDto> getMessages(Long chatId, Principal principal);


}
