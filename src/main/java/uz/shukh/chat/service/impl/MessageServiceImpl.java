package uz.shukh.chat.service.impl;


import org.springframework.stereotype.Service;
import uz.shukh.chat.dto.MessageDto;
import uz.shukh.chat.dto.MessageForm;
import uz.shukh.chat.dto.ResultDto;
import uz.shukh.chat.entity.Chat;
import uz.shukh.chat.entity.Message;
import uz.shukh.chat.entity.User;
import uz.shukh.chat.repository.ChatRepository;
import uz.shukh.chat.repository.MessageRepository;
import uz.shukh.chat.repository.UserRepository;
import uz.shukh.chat.service.MessageService;

import javax.persistence.EntityManager;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {
    private final MessageRepository repository;
    private final ChatRepository chatRepository;
    private final EntityManager manager;
    private final UserRepository userRepository;

    public MessageServiceImpl(MessageRepository repository, ChatRepository chatRepository, EntityManager manager, UserRepository userRepository) {
        this.repository = repository;
        this.chatRepository = chatRepository;
        this.manager = manager;
        this.userRepository = userRepository;
    }

    @Override
    public Object sendMessage(MessageForm form, Principal principal) throws Exception {
        Optional<Chat> chat = chatRepository.findById(form.chatId);
        if (chat.isEmpty()) return new ResultDto(false, "Chat topilmadi: " + form.chatId);
        User user = userRepository.findFirstByUsername(principal.getName());
        Message message = new Message();
        message.setText(form.getText());
        message.setFromUser(user);
        if (Objects.equals(user.id, chat.get().getToUser().id)){
            message.setToUser(chat.get().getMeUser());
        }else {
            message.setToUser(chat.get().getToUser());
        }

        message.setChat(chat.get());

        repository.save(message);
        return new ResultDto();
    }

    @Override
    public List<MessageDto> getMessages(Long chatId, Principal principal) {
        return repository.findAllByChatId(chatId).stream().map(message-> MessageDto.toDto(message,principal)).collect(Collectors.toList());
    }
}
