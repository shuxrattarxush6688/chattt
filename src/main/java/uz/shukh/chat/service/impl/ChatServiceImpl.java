package uz.shukh.chat.service.impl;

import org.springframework.stereotype.Service;
import uz.shukh.chat.dto.ChatDto;
import uz.shukh.chat.dto.ChatForm;
import uz.shukh.chat.dto.ResultDto;
import uz.shukh.chat.entity.Chat;
import uz.shukh.chat.entity.User;
import uz.shukh.chat.repository.ChatRepository;
import uz.shukh.chat.repository.UserRepository;
import uz.shukh.chat.service.ChatService;

import javax.persistence.EntityManager;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ChatServiceImpl implements ChatService {
    private final ChatRepository repository;
    private final EntityManager manager;
    private final UserRepository userRepository;

    public ChatServiceImpl(ChatRepository repository, EntityManager manager, UserRepository userRepository) {
        this.repository = repository;
        this.manager = manager;
        this.userRepository = userRepository;
    }


    @Override
    public Object create(ChatForm form, Principal principal) {
        User me = userRepository.findFirstByUsername(principal.getName());
        Optional<User> toUser = userRepository.findById(form.getToId());
        if (toUser.isEmpty()) return new ResultDto(false, "User topilmadi!");
        if (me != null) {
            Chat chat = repository.findByMeUserIdAndToUserId(me.id, form.toId);
            if (chat ==null){
            chat = repository.findByMeUserIdAndToUserId(form.toId, me.id);
            }
            if (chat == null) {
                chat = new Chat();
                chat.setMeUser(me);
                chat.setName(toUser.get().getFullName());
                chat.setTo(manager.getReference(User.class, form.getToId()));
                repository.save(chat);
            }

            return ChatDto.toDto(chat);

        } else return new ResultDto(false, "Tizimga qayta kiring");
    }

    @Override
    public List<ChatDto> findAll(Principal principal) {
        return repository.findAllByMeUserUsername(principal.getName()).stream().map(ChatDto::toDto).collect(Collectors.toList());
    }
}
