package uz.shukh.chat.service.impl;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.shukh.chat.dto.UserDto;
import uz.shukh.chat.dto.UserForm;
import uz.shukh.chat.entity.User;
import uz.shukh.chat.enums.Status;
import uz.shukh.chat.repository.UserRepository;
import uz.shukh.chat.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDto add(UserForm form) {
        if (repository.existsByUsername(form.getUsername())){
            throw new RuntimeException("This username already exists");
        }
        User user = new User();
        user.setFullName(form.getFullName());
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        user.setUsername(form.getUsername());
        user.setStatus(Status.ONLINE);
        return UserDto.toDto(repository.save(user));
    }

    @Override
    public List<UserDto> fidAllByOnlineUsers() {
        return repository.findAllByStatus(Status.ONLINE).stream().map(UserDto::toDto).collect(Collectors.toList());
    }

    @Override
    public UserDto update(UserForm form, Long id) {
        User user = repository.getById(id);
        user.setUsername(form.getUsername());
        user.setFullName(form.getFullName());
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        return UserDto.toDto(repository.save(user));
    }

    @Override
    public void statusOnOff(Long id) {
        User user = repository.getById(id);
        if (user.getStatus().equals(Status.ONLINE)){
            user.setStatus(Status.OFFLINE);
        }else {
            user.setStatus(Status.ONLINE);
        }
        repository.save(user);

    }

}
